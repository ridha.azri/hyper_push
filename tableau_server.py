from tableau_api_lib import TableauServerConnection
from tableau_api_lib.utils.querying import get_projects_dataframe
import logging
from auth import AuthConfig


class TableauServices:
    def __init__(self, path_to_hyper, data_source_name, project_name):
        self.path_to_hyper = path_to_hyper
        self.data_source_name = data_source_name
        self.project_name = project_name
        self.project_id = None
        self.config_with_token = AuthConfig().credentials

    def login_to_tableau(self):
        tableau_connection = None
        try:
            tableau_connection = TableauServerConnection(config_json=self.config_with_token, env='tableau_prod')
            tableau_connection.sign_in()
            logging.info("login status: {}".format(tableau_connection.server_info().json()))
        except Exception as ex:
            logging.error('an error has occured when try to login to tableau cloud server, error:{}'.format(ex))

        project_id = None
        projects_df = get_projects_dataframe(tableau_connection)
        names = projects_df[['name', 'id']].to_dict()['name']
        ids = projects_df[['name', 'id']].to_dict()['id']
        for k, v in names.items():
            if self.project_name == v:
                project_id = ids.get(k, None)
                logging.info("the id of the project_name :{} is :{}".format(self.project_name, project_id))
                self.project_id = project_id
                break
        if project_id is None:
            logging.error("the id project_name :{} is  not found".format(self.project_name))
        return tableau_connection, project_id

    def push_hyper(self, conn):
        response = conn.publish_data_source(datasource_file_path=self.path_to_hyper,
                                            datasource_name=self.data_source_name,
                                            project_id=self.project_id)
        logging.info(response.json())
        if response.json().get('datasource', None) is not None:
            logging.info('the datasource has been created/ updated')
            info = response.json().get('datasource', None)
            logging.info('datasource url: {}'.format(info.get('webpageUrl', None)))
        conn.sign_out()

    def run_push(self):
        connection, _ = self.login_to_tableau()
        if self.project_id is None:
            logging.warning('cannot push hyper file to unknown project, the id of your project is not found')
            exit(1)
        else:
            self.push_hyper(connection)


if __name__ == '__main__':
    publish = TableauServices('customer.hyper', 'customers', 'Badc à Sable')
    conn = publish.run_push()
