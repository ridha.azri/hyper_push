import os
from hurry.filesize import size
from pathlib import Path
from tableauhyperapi import HyperProcess, Telemetry, Connection, CreateMode, NOT_NULLABLE,\
    NULLABLE, SqlType, TableDefinition, \
    Inserter, \
    escape_name, escape_string_literal, \
    HyperException

import logging
from schema import COLS_LIST, COLS_TYPE


class HyperGenerator:

    def __init__(self, table_name, path_to_csv, path_to_hyper):
        self.table_name = table_name
        self.path_to_csv = path_to_csv
        self.path_to_hyper = path_to_hyper
        self.process_parameters = {
            # Limits the number of Hyper event log files to two.
            "log_file_max_count": "2",
            # Limits the size of Hyper event log files to 100 megabytes.
            "log_file_size_limit": "100M"
        }

    def generate_table_schema(self):
        logging.info('generate schema to be used in table definition process')
        my_dict = dict(zip(COLS_LIST, COLS_TYPE))
        columns = []
        for k, v in my_dict.items():
            if v == 'text':
                logging.info("you tell that: {} is {} type".format(k, v))
                columns.append(TableDefinition.Column(k, SqlType.text(), NOT_NULLABLE))
            elif v == 'int':
                logging.info("you tell that: {} is {} type".format(k, v))
                columns.append(TableDefinition.Column(k, SqlType.int(), NOT_NULLABLE))
            elif v == 'date':
                logging.info("you tell that: {} is {} type".format(k, v))
                columns.append(TableDefinition.Column(k, SqlType.date(), NOT_NULLABLE))
            elif v == 'double':
                logging.info("you tell that: {} is {} type".format(k, v))
                columns.append(TableDefinition.Column(k, SqlType.double(), NOT_NULLABLE))
            elif v == 'bool':
                logging.info("you tell that: {} is {} type".format(k, v))
                columns.append(TableDefinition.Column(k, SqlType.bool(), NOT_NULLABLE))
            # add new schema type as you want
            else:
                logging.info("the type of this field :  {} is not yet set, it will be text by default".format(k))
                columns.append(TableDefinition.Column(k, SqlType.text(), NOT_NULLABLE))
        return columns

    def create_table_definition(self):
        table_definition = TableDefinition(table_name=self.table_name,columns=self.generate_table_schema())
        return table_definition

    def run_create_hyper_file_from_csv(self):
        logging.info("Load data from CSV into table in Hyper file")
        path_to_database = Path(self.path_to_hyper)
        def_table = self.create_table_definition()
        with HyperProcess(telemetry=Telemetry.SEND_USAGE_DATA_TO_TABLEAU, parameters=self.process_parameters) as hyper:
            connection_parameters = {"lc_time": "en_US"}
            with Connection(endpoint=hyper.endpoint,
                            database=path_to_database,
                            create_mode=CreateMode.CREATE_AND_REPLACE,
                            parameters=connection_parameters) as connection:
                connection.catalog.create_table(table_definition=def_table)
                csv_file_size = os.path.getsize(self.path_to_csv)
                logging.info("the size of your csv file is : {}".format(size(csv_file_size)))
                logging.info("to load the csv file into the table. Since the first line")
                logging.info("of our csv file contains the column names, we use the `header` option to skip it.")
                count_in_customer_table = connection.execute_command(
                    command=f"COPY {def_table.table_name} from {escape_string_literal(self.path_to_csv)} with "
                            f"(format csv, NULL 'NULL', delimiter ',', header)")
                logging.info(f"The number of rows in table {def_table.table_name} is {count_in_customer_table}.")
            logging.info("The connection to the Hyper file has been closed.")
        logging.info("The Hyper process has been shut down.")
        file_size = os.path.getsize(self.path_to_hyper)
        logging.info("the size of your hyper file is: {}".format(size(file_size)))
        return self.path_to_hyper
