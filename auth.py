import logging
import os
import json


class AuthConfig:
    def __init__(self):
        self.credentials = {
            'tableau_prod': {
                    'server': os.environ.get('TABLEAU_SERVER', default=''),
                    'api_version': os.environ.get('TABLEAU_API_VERSION', default=''),
                    'personal_access_token_name': os.environ.get('TABLEAU_TOKEN_NAME', default=''),
                    'personal_access_token_secret': os.environ.get('TABLEAU_ACCES_TOKEN', default=''),
                    'site_name': os.environ.get('TABLEAU_SITE_NAME', default=''),
                    'site_url': os.environ.get('TABLEAU_SITE_URL', default='')
            }
    }
        logging.info('credentials loaded')

