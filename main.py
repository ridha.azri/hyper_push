import time
from hyper_generator import HyperGenerator
from tableau_server import TableauServices
import logging


class AutomatePush:
    def __init__(self, table_def, csv_file_path, hyper_file_path, tbl_data_source_name,tbl_project_name):
        self.hyper_generator = HyperGenerator(table_def, csv_file_path, hyper_file_path)
        self.tableau_service = TableauServices(hyper_file_path, tbl_data_source_name, tbl_project_name)

    def run_automation(self):
        self.hyper_generator.run_create_hyper_file_from_csv()
        self.tableau_service.run_push()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s | %(levelname)s | %(name)s : %(message)s')
    logging.getLogger('urllib3.connectionpool').setLevel(logging.ERROR)
    logging.getLogger('tableau.endpoint').setLevel(logging.ERROR)
    try:
        start = time.time()
        automate = AutomatePush('Customer', 'data/customer.csv', 'customer.hyper', 'customers', 'Bac à Sable')
        automate.run_automation()
        end = time.time()
        logging.info("the process take: {} min".format((end - start) /60))
    except Exception as ex:
        logging.info(ex)
        exit(1)


